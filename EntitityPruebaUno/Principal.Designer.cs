﻿namespace EntitityPruebaUno
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AgregarUsuarioButton = new System.Windows.Forms.Button();
            this.CurpField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ApellidosField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NombresField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UsuariosListView = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AgregarUsuarioButton);
            this.groupBox1.Controls.Add(this.CurpField);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ApellidosField);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.NombresField);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(201, 248);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del nuevo Usuario";
            // 
            // AgregarUsuarioButton
            // 
            this.AgregarUsuarioButton.Location = new System.Drawing.Point(66, 185);
            this.AgregarUsuarioButton.Name = "AgregarUsuarioButton";
            this.AgregarUsuarioButton.Size = new System.Drawing.Size(75, 23);
            this.AgregarUsuarioButton.TabIndex = 6;
            this.AgregarUsuarioButton.Text = "Agregar";
            this.AgregarUsuarioButton.UseVisualStyleBackColor = true;
            // 
            // CurpField
            // 
            this.CurpField.Location = new System.Drawing.Point(7, 139);
            this.CurpField.Name = "CurpField";
            this.CurpField.Size = new System.Drawing.Size(187, 20);
            this.CurpField.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CURP:";
            // 
            // ApellidosField
            // 
            this.ApellidosField.Location = new System.Drawing.Point(6, 95);
            this.ApellidosField.Name = "ApellidosField";
            this.ApellidosField.Size = new System.Drawing.Size(188, 20);
            this.ApellidosField.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Apellidos:";
            // 
            // NombresField
            // 
            this.NombresField.Location = new System.Drawing.Point(6, 51);
            this.NombresField.Name = "NombresField";
            this.NombresField.Size = new System.Drawing.Size(188, 20);
            this.NombresField.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombres:";
            // 
            // UsuariosListView
            // 
            this.UsuariosListView.GridLines = true;
            this.UsuariosListView.Location = new System.Drawing.Point(220, 12);
            this.UsuariosListView.Name = "UsuariosListView";
            this.UsuariosListView.Size = new System.Drawing.Size(473, 248);
            this.UsuariosListView.TabIndex = 1;
            this.UsuariosListView.UseCompatibleStateImageBehavior = false;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 291);
            this.Controls.Add(this.UsuariosListView);
            this.Controls.Add(this.groupBox1);
            this.Name = "Principal";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox NombresField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CurpField;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ApellidosField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AgregarUsuarioButton;
        private System.Windows.Forms.ListView UsuariosListView;
    }
}

