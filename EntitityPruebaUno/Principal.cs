﻿using Entidades;
using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EntitityPruebaUno
{
    public partial class Principal : Form
    {
        UsuarioCN controlador; 

        public Principal()
        {
            InitializeComponent();
            AgregarUsuarioButton.Click += new EventHandler(this.AgregarUsuarioButton_Click);
            this.ListarDatos();
        }

        private void AgregarUsuarioButton_Click(object sender, EventArgs e)
        {
            this.AgregarUsuario();
        }

        private void AgregarUsuario()
        {
            Usuario usuario = new Usuario();
            usuario.nombre = NombresField.Text;
            usuario.apellidos = ApellidosField.Text;
            usuario.curp = CurpField.Text;

            this.inicializarControlador();
            controlador.AddUsuario(usuario);
            this.ListarDatos();

            this.CleanFields();
            MessageBox.Show("ok");
        }

        private void CleanFields()
        {
            NombresField.Text = "";
            ApellidosField.Text = "";
            CurpField.Text = "";
        }

        private void ListarDatos()
        {
            this.inicializarControlador();
            UsuariosListView.View = View.Details;
            UsuariosListView.FullRowSelect = true;

            UsuariosListView.Columns.Clear();
            UsuariosListView.Items.Clear();

            var usuarios = controlador.GetUsuarios();
            UsuariosListView.Columns.Add("ID", 100);
            UsuariosListView.Columns.Add("Nombres", 100);
            UsuariosListView.Columns.Add("Price", 100);
            UsuariosListView.Columns.Add("CURP", 100);
            
            foreach (var usuario in usuarios)
            {
                String [] row = {usuario.UserId.ToString(), usuario.nombre,usuario.apellidos, usuario.curp };
                this.addItem(row);
            }

        }

        private void addItem(String[] row)
        {
            ListViewItem item = new ListViewItem(row);
            UsuariosListView.Items.Add(item);
        }

        private UsuarioCN inicializarControlador()
        {
            return controlador = new UsuarioCN();
        }
    }
}
