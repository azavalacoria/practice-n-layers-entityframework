﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;
using Entidades;

namespace Negocio
{
    public class UsuarioCN
    {
        UsuarioDALC repositorio;

        public List<Usuario> GetUsuarios()
        {
            if(repositorio == null)
                repositorio = new UsuarioDALC();
            return repositorio.GetUsuarios();
        }

        public void AddUsuario(Usuario usuario)
        {
            if (repositorio == null)
                repositorio = new UsuarioDALC();
            repositorio.InsertUsuario(usuario);
        }
    }
}
