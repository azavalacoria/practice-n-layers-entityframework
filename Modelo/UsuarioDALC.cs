﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Entidad;

namespace Modelo
{
    public class UsuarioDALC
    {
        public List<Usuario> GetUsuarios()
        {
            using (DB_EjemploEntities db = new DB_EjemploEntities())
                return db.Usuario.ToList();
        }

        public void InsertUsuario(Usuario usuario)
        {
            using (DB_EjemploEntities db = new DB_EjemploEntities()) {
                db.Usuario.Add(usuario);
                db.SaveChanges();
            }

        }
    }
}
